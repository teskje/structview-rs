# Changelog

All notable changes to this project will be documented in this file.

## 1.1.0 (2020-06-05)

### Added

- Two new trait methods for viewing slices of `View` types:
  `View::view_slice` and `View::view_boxed_slice` (!1)


## 1.0.0 (2020-03-15)

First stable release.
