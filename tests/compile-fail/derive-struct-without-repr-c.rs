extern crate structview;

use structview::{u16_be, View};

#[derive(Clone, Copy, View)] //~ ERROR types that derive `structview::View` must be repr(C)
struct Test {
    foo: [u8; 4],
    bar: u16_be,
}

fn main() {}
