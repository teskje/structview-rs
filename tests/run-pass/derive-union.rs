extern crate structview;

use structview::{u16_be, View};

#[derive(Clone, Copy, View)]
#[repr(C)]
union Test {
    foo: [u8; 4],
    bar: u16_be,
}

fn main() {}
